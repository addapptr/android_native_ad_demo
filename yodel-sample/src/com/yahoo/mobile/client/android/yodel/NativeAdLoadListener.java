package com.yahoo.mobile.client.android.yodel;

public interface NativeAdLoadListener {

    void nativeAdLoaded();

    void nativeAdFailedToLoad();
}
