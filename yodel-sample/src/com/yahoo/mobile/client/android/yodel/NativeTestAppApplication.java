/*
 * Copyright 2015 Yahoo Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//TODO: copyrights?

package com.yahoo.mobile.client.android.yodel;

import android.app.Application;
import android.content.Context;
import android.net.http.HttpResponseCache;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.multidex.MultiDex;

import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.BannerPlacementLayout;
import com.intentsoftware.addapptr.PlacementSize;
import com.yahoo.mobile.client.android.yodel.utils.AnalyticsHelper;
import com.yahoo.mobile.client.share.search.settings.SearchSDKSettings;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NativeTestAppApplication extends Application implements AATKit.Delegate {
    private static String LOG_TAG = NativeTestAppApplication.class.getSimpleName();

    public static final String YAHOO_SEARCH_APIKEY = "your_api_key";

    private static NativeTestAppApplication sApplication;

    private Handler mMainThreadHandler;

    private int nativePlacementId = -1;

    private List<NativeAdLoadListener> nativeAdLoadListeners = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Init Search SDK
        SearchSDKSettings.initializeSearchSDKSettings(
                new SearchSDKSettings.Builder(YAHOO_SEARCH_APIKEY)
                        .setVoiceSearchEnabled(true));

        sApplication = this;

        // create handlers
        mMainThreadHandler = new Handler(Looper.getMainLooper());
        HandlerThread backgroundHandlerThread = new HandlerThread(LOG_TAG);
        backgroundHandlerThread.start();

        // http response cache
        File httpCacheDir = new File(getCacheDir(), "http");
        long httpCacheSize = 100 * 1024 * 1024; // 100 MiB

        try {
            HttpResponseCache.install(httpCacheDir, httpCacheSize);
        } catch (IOException e) {
            AnalyticsHelper.logError(LOG_TAG, "HTTP response cache installation failed", e);
        }

        // Init AATKit
        AATKit.init(this, this);
        nativePlacementId = AATKit.createPlacement("Native", PlacementSize.Native);
    }

    public static NativeTestAppApplication getInstance() {
        return sApplication;
    }

    public Handler getMainThreadHandler() {
        return mMainThreadHandler;
    }

    public void addNativeAdLoadListener(NativeAdLoadListener listener) {
        nativeAdLoadListeners.add(listener);
    }

    public void removeNativeAdLoadListener(NativeAdLoadListener listener) {
        nativeAdLoadListeners.remove(listener);
    }


    @Override
    public void aatkitHaveAd(int placementId) {
        if (placementId == nativePlacementId) {
            for (NativeAdLoadListener listener : nativeAdLoadListeners) {
                listener.nativeAdLoaded();
            }
        }
    }

    @Override
    public void aatkitNoAd(int placementId) {
        if (placementId == nativePlacementId) {
            for (NativeAdLoadListener listener : nativeAdLoadListeners) {
                listener.nativeAdFailedToLoad();
            }
        }
    }

    @Override
    public void aatkitPauseForAd(int i) {
    }

    @Override
    public void aatkitResumeAfterAd(int i) {
    }

    @Override
    public void aatkitShowingEmpty(int i) {
    }

    @Override
    public void aatkitUserEarnedIncentive(int i) {
    }

    @Override
    public void aatkitObtainedAdRules(boolean b) {
    }

    @Override
    public void aatkitUnknownBundleId() {
    }

    @Override
    public void aatkitHaveAdForPlacementWithBannerView(int i, BannerPlacementLayout bannerPlacementLayout) {

    }

    public int getNativePlacementId() {
        return nativePlacementId;
    }
}
