/*
 * Copyright 2015 Yahoo Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yahoo.mobile.client.android.yodel;

import android.util.Log;

import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.ad.NativeAdData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Handles fetching, caching, and destroying of native ads.</p>
 */
public class NativeAdFetcher implements NativeAdLoadListener {
    private static final String LOG_TAG = NativeAdFetcher.class.getSimpleName();
    /**
     * Maximum number of ads to prefetch.
     */
    private static final int PREFETCHED_ADS_SIZE = 5;
    /**
     * Maximum number of times to try fetch an ad after failed attempts.
     */
    private static final int MAX_FETCH_ATTEMPT = 4;
    private List<AdNativeListener> mAdNativeListeners = new ArrayList<>();
    private Map<Integer, NativeAdData> adMapAtIndex = new HashMap<>();
    private List<Integer> positionsWithReportedAdSpaces = new ArrayList<>();
    private int numberOfPrefetchedAds;
    private int totalNumberOfFetchedAds;
    private int mFetchFailCount;
    private int nativePlacementId = -1;
    private NativeTestAppApplication application;

    public NativeAdFetcher(NativeTestAppApplication application) {
        this.nativePlacementId = application.getNativePlacementId();
        application.addNativeAdLoadListener(this);
        this.application = application;
    }

    @Override
    public void nativeAdLoaded() {

        Log.i(LOG_TAG, "onFetched");

        numberOfPrefetchedAds++;
        totalNumberOfFetchedAds++;
        mFetchFailCount = 0;
        ensurePrefetchAmount();

        notifyObserversOfAdSizeChange();
    }

    @Override
    public void nativeAdFailedToLoad() {
        Log.i(LOG_TAG, "onFetchFailed");

        mFetchFailCount++;
        ensurePrefetchAmount();
    }

    /**
     * Adds an {@link AdNativeListener} that would be notified for any changes to the native ads
     * list.
     *
     * @param listener the listener to be notified
     */
    public synchronized void addListener(AdNativeListener listener) {
        mAdNativeListeners.add(listener);
    }

    /**
     * Gets native ad at a particular index in the fetched ads list.
     *
     * @param index the index of ad in the fetched ads list
     * @return the native ad in the list
     * @see #getFetchedAdsCount()
     */
    public synchronized NativeAdData getAdForIndex(final int index) {
        NativeAdData adNative = adMapAtIndex.get(index);

        if (adNative == null && numberOfPrefetchedAds > 0) {
            numberOfPrefetchedAds--;
            adNative = AATKit.getNativeAd(nativePlacementId);

            if (adNative != null) {
                adMapAtIndex.put(index, adNative);
            }
        }

        ensurePrefetchAmount(); // Make sure we have enough pre-fetched ads

        return adNative;
    }

    /**
     * Reports ad space for a particular position.
     * @param position the position in list
     */
    public synchronized void reportAdSpaceForPosition(final int position) {
        if (!positionsWithReportedAdSpaces.contains(position)) { //report adspace only once per position (as we keep the same ad for reuse in adMapAtIndex)
            positionsWithReportedAdSpaces.add(position);
            AATKit.reportAdSpaceForNativePlacement(nativePlacementId);
        }
    }

    /**
     * Gets the number of ads that have been fetched so far.
     *
     * @return the number of ads that have been fetched
     */
    public synchronized int getFetchedAdsCount() {
        return totalNumberOfFetchedAds;
    }

    /**
     * Fetches a new native ad.
     *
     * @see #destroyAllAds()
     */
    public synchronized void prefetchAds() {
        fetchAd();
    }

    /**
     * Destroys ads that have been fetched, that are still being fetched and removes all resource
     * references that this instance still has. This should only be called when the Activity that
     * is showing ads is closing, preferably from the {@link android.app.Activity#onDestroy()}.
     * </p>
     * The converse of this call is {@link #prefetchAds()}.
     */
    public synchronized void destroyAllAds() {
        for (NativeAdData nativeAd : adMapAtIndex.values()) {
            AATKit.detachNativeAdFromLayout(nativeAd);
        }

        mFetchFailCount = 0;
        adMapAtIndex.clear();

        numberOfPrefetchedAds = 0;
        totalNumberOfFetchedAds = 0;

        notifyObserversOfAdSizeChange();
        application.removeNativeAdLoadListener(this);
    }

    /**
     * Notifies all registered {@link AdNativeListener} of a change in ad data count.
     */
    private void notifyObserversOfAdSizeChange() {
        for (AdNativeListener listener : mAdNativeListeners) {
            listener.onAdCountChanged();
        }
    }

    /**
     * Fetches a new native ad.
     */
    private synchronized void fetchAd() {

        boolean startedLoading = AATKit.reloadPlacement(nativePlacementId);
        if (!startedLoading) {
            mFetchFailCount++;
        }
    }

    /**
     * Ensures that the necessary amount of prefetched native ads are available.
     */
    private synchronized void ensurePrefetchAmount() {
        if (numberOfPrefetchedAds < PREFETCHED_ADS_SIZE &&
                (mFetchFailCount < MAX_FETCH_ATTEMPT)) {
            fetchAd();
        }
    }

    /**
     * Determines if the native ad can be used.
     *
     * @param adNative the native ad object
     * @return <code>true</code> if the ad object can be used, false otherwise
     */
    private boolean canUseThisAd(NativeAdData adNative) {
        return adNative != null && AATKit.isNativeAdReady(adNative) && !AATKit.isNativeAdExpired(adNative);
    }


    /**
     * Listener that is notified when changes to the list of fetched native ads are made.
     */
    public interface AdNativeListener {
        /**
         * Triggered when the number of ads have changed. Adapters that implement this class
         * should notify their data views that the dataset has changed.
         */
        void onAdCountChanged();
    }
}
